#include <time.h>
#include <iostream>
using namespace std;

int tab[]={2,86,87,102,39};


void selectionSort(int tab[], int size){
	// selectionSort
    int posMin;
    int min;
    for(int i=0; i<size; i++){
        posMin=i;
        min=tab[i];
        for(int j=i; j<size; j++){
            if(tab[j]<min){
                min=tab[j];
                posMin=j;
            }
        }
        int a = tab[i];
        tab[i]=min;
        tab[posMin]=a;
    }
}

void affiche(int tab[], int size){
    for(int i=0; i<size; i++){
        cout<<tab[i]<<endl;
    }
}

int main()
{
    int tab[]={2,86,5,102,39};
    selectionSort(tab, 5);
    affiche(tab,5);
    
	return 0;
}
