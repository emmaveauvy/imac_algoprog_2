#include <iostream>
using namespace std;

int tab[5]={70,23,50,45,1234};


bool egualite(int tab[], int size){
    int element = tab[0];
    for (uint i = 1 ; i < size ; i++){
        if(tab[i] != element){
            return false;
        }
    }
    return true;
}




void recursivQuickSort(int tab[], int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)

	if(size<2 || egualite(tab, size)){

		return;
	}
	
	int lowerSize = 0;
	int  greaterSize = 0; // effectives sizes

	// split
	int split= tab[rand()%size];
	int petitTab[5];
	int grandTab[5];



	for(int i=0; i<size; i++){
		if(tab[i] < split){
            petitTab[lowerSize]=tab[i];
            lowerSize++;
        }else {
            grandTab[greaterSize]=tab[i];
            greaterSize++;
        }

	}

	// recursiv sort of lowerArray and greaterArray
	recursivQuickSort(petitTab, lowerSize);
    recursivQuickSort(grandTab, greaterSize);

	// merge
	tab[lowerSize]=split;

	 for (int i=0 ; i<lowerSize ; i++){
        tab[i]=petitTab[i];
    }
    for (int i = 1 ; i< greaterSize ; i++){
        tab[i+lowerSize]=grandTab[i];
    }

}

void quickSort(int tab[], int size){
	//recursivQuickSort(toSort, toSort.size());
	recursivQuickSort(tab, size);
}

void affiche(int tab[], int size){
    for(int i=0; i<size; i++){
        cout<<tab[i]<<endl;
    }
}



int main()
{
	recursivQuickSort(tab,5);
	affiche(tab,5);
	return 0;
}
