#include <iostream>
using namespace std;

int tab[5]={70,23,50,45,1234};
int result[5];
void insertionSort(int tab[],int size, int result[]);
void insert(int tab[], int index, int val, int size);


void affiche(int tab[], int size){
    for(int i=0; i<size; i++){
        cout<<tab[i]<<endl;
    }
}

int main(){
	insertionSort(tab, 5, result);
    affiche(result,5);
	return 0;
}

void insertionSort(int tab[],int size, int result[]){
	result[0]=tab[0];
	int tailleResult=1;
	if(size>0){
		for(int i=1; i<size; i++){
			for(int j=0; j<tailleResult; j++){
				if(result[j]>tab[i]){
					//result[j]=tab[i];
					insert(result, j, tab[i], tailleResult);
					break;
				}else{
					result[tailleResult]=tab[i];
				}
			}
			tailleResult++;
		}
	}
}

void insert(int tab[], int index, int val, int size){
	for (int i = size; i > index; i--){
		tab[i] = tab[i-1];
	}
	tab[index] = val;
}