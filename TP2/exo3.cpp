#include <iostream>
using namespace std;

int tab[5]={70,23,50,45,1234};

void bubbleSort(int tab[], int size){
	for(int i=0; i<size; i++){
		 if(i != size - 1) {
            if(tab[i] > tab[i+1]) {
                int a = tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = a;
            }
        }
	}
}

void affiche(int tab[], int size){
    for(int i=0; i<size; i++){
        cout<<tab[i]<<endl;
    }
}

int main()
{
	bubbleSort(tab, 5);

    affiche(tab,5);

	return 0;
}
