#include <iostream>
using namespace std;
int tab[5]={70,23,50,45,1234};

void merge(int first[], int sizefirst, int second[], int sizesecond,int result[], int size)
{
	int i =0;
    int j = 0;
    while(i+j<size){
        if(i == sizefirst){
            result[i+j] = second[j];
            j++;
        }else if(j == sizesecond){
            result[i+j] = first[i];
            i++;
        }else if(first[i] <= second[j]){
            result[i+j] = first[i];
            i++;
        }else if(second[j] < first[i]){
            result[i+j] = second[j];
            j++;
        }
    }

}

void splitAndMerge(int origin[], int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if(size<2){
		return;
	}

	// initialisation
	int first[size/2];
	int second[size-(size/2)];
	
	// split
	int taille= size;

	 for(int i=0 ; i<size/2 ; i++){
        first[i]=origin[i];
    }

    for(int i=0 ; i<size-(size/2) ; i++){
        second[i]=origin[i+(size/2)];
    }

    //recursive call of splitAndMerge
    splitAndMerge(first, size/2);
    splitAndMerge(second, size-(size/2));

	// merge

    merge(first,size/2, second,size-(size/2), origin, size);

}

	



void mergeSort(int tab[], int size)
{
    splitAndMerge(tab, size);
}

int main()

{

	mergeSort(tab, 5);
	return 0;
}
