#include <iostream>
using namespace std;

int tab[5]={23,45,50,70,1234};


int binarySearch(int tab[], int toSearch, int size)
{
	int start = 0;
	int end=size;
	int mid;
	int found;

	while(start<end){
		mid=(start+end)/2;
		if(toSearch>tab[mid]){
			start=mid+1;
		}else{
			if(toSearch<tab[mid]){
				end=mid;
			}else{
				found=mid;
				return found;
			}
		}
	}
	return -1;
}

int main()
{
	cout<<binarySearch(tab,30,5);

	return 0;
}
