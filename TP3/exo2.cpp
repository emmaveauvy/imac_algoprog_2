#include <iostream>
using namespace std;

int tab[5]={23,45,46,50,60};

void binarySearchAll(int tab[], int toSearch, int *indexMin, int *indexMax, int size)
{
	int start = 0;
	int end=size;
	int mid;
	int found;
	*indexMin = *indexMax = -1;

	while(start<end){
		mid=(start+end)/2;
		if(toSearch>tab[mid]){
			start=mid+1;
		}else{
			if(toSearch<tab[mid]){
				end=mid;
			}else{
				*indexMin=mid;
				end=mid;
			}
		}
	}

	start = 0;
	end=size;
	while(start<end){
		mid=(start+end)/2;
		if(toSearch>tab[mid]){
			start=mid+1;
		}else{
			if(toSearch<tab[mid]){
				end=mid;
			}else{
				*indexMax=mid;
				start=mid+1;
			}
		}
	}
}

int main()
{
	int indexMin;
	int indexMax;
	binarySearchAll(tab, 232, &indexMin,&indexMax, 5);
	cout<<indexMin<<endl;
	cout<<indexMax<<endl;
	return 0;
}
