#include <iostream>
using namespace std;

/*allEvens
rempli evens avec tous les
nombres paires de array. evenSize représente le nombre de valeur dans evens (est donc égale 0 au début) et arraySize est le nombre de valeur dans array.*/

void allEvens(int evens[], int array[], int *evenSize, int arraySize)
{
   if (arraySize>0){
       if(array[arraySize-1]%2==0){
           evens[*evenSize]=array[arraySize-1];
           (*evenSize)++;
            return allEvens(evens, array, evenSize, arraySize-1);

       }
        return allEvens(evens, array, evenSize, arraySize-1);
   }
}

int main()
{
    int evenSize=0;
    int tab1[4]={23,12,4,8};
    int tab2[4];
    allEvens(tab2, tab1, &evenSize, 4);
    for(int i=0; i<evenSize; i++){
        cout<<tab2[i]<<endl;
    }

    return 0;
}




