#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    Noeud* apres;
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
};


void initialise(Liste* liste)
{
    //on initiliase la liste
    liste->premier=nullptr;

}

bool est_vide(const Liste* liste)
{
    //si la liste est vide on retourne true
    if(liste->premier==nullptr){
        return true;
    }else{

   return false;

    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* newNoeud= new Noeud;
    newNoeud->donnee=valeur; 
    newNoeud->suivant=nullptr;

    
    if(liste==NULL){
        liste->premier=newNoeud;
    }else{

        Noeud * enCours = liste->premier;
        
        while(enCours->suivant!=nullptr){
            enCours = enCours->suivant;
        }
        enCours->suivant = newNoeud;
    }
}

void affiche(const Liste* liste)
{
   
    Noeud* enCours=liste->premier;
    
    //tant que la valeur du noeud n'est pas null on la cout
    while(enCours!=nullptr){
        cout<<enCours->donnee<<endl;
        //on affecte au noeud la valeur suivante
        enCours=enCours->suivant;
    }

}

int recupere(const Liste* liste, int n)
{
    int i=0;
    Noeud* enCours=liste->premier;

    //tant que le noeud n'est pas vide (fin de la liste) ou que le compteur n'est pas n on passe au noeud de la liste suivant

    while (enCours!=nullptr || i>n)
    {
        //Si i=n il faut retourner la valeur du noeud en cours et ne pas aller plus loin
        if(i==n){
        return enCours->donnee;
        }
        enCours=enCours->suivant;
        i++;
    }
    return 0;
}

int cherche(const Liste* liste, int valeur)
{
    //quasi meme principe que pour return
    int i=1;
    Noeud* enCours=liste->premier;
     while (enCours!=nullptr)
    {
        if(enCours->donnee==valeur){
        return i;
        }
        enCours=enCours->suivant;
        i++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    //ici on veut stocker valeur dans le noeud n
    int i=0;

     Noeud* enCours=liste->premier;
     while (enCours!=nullptr || i<n)
    {
        //une fois qu'on est arrivé au noeud n on peut y stocker la valeur
        if(i==n){
            enCours->donnee=valeur;
            break;
        }
        enCours=enCours->suivant;
        i++;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{ 
    if(tableau->capacite>tableau->taille){
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille++;
    }else{

        int *sauvTab = new int[tableau->capacite+1];
        for(int i=0 ; i<tableau->taille ; i++){
            sauvTab[i]=tableau->donnees[i];
        }
        delete tableau->donnees;
        tableau->donnees = sauvTab;
        tableau->donnees[tableau->taille] = valeur;
        tableau->capacite+=1;
        tableau->taille+=1;
    }
}

void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite=capacite;
    tableau->donnees = new int[capacite];
    tableau->taille=0;
}

bool est_vide(const DynaTableau* liste)
{
    if(liste->taille==0){
       return true; 
    }else{
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    //on parcourt le tableau et à chaque tour on affiche la valeur correspondante.
    for(int i=0; i<tableau->taille; i++){
        cout<<"Donnée "<<i<<" : "<<tableau->donnees;
    }

}

int recupere(const DynaTableau* tableau, int n)
{
    if(!est_vide(tableau)){
        return tableau->donnees[n];
    }
    else{
        return 0;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->taille; i++){
        if(tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int nvPrem = 0;
    Noeud *aSuppr = liste->premier;
    nvPrem = aSuppr->donnee;
    liste->premier = aSuppr->suivant;
    delete aSuppr;
    return nvPrem;

}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud* newNoeud= new Noeud;
    newNoeud->donnee=valeur;
    newNoeud->suivant=liste->premier;
    liste->premier=newNoeud;

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    int nvPrem = 0;
    Noeud* premierSuppr = liste->premier;

    nvPrem = premierSuppr->donnee;
    liste->premier = premierSuppr->suivant;
    delete premierSuppr;
    
    return nvPrem;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
       cout << "Oups y a une anguille dans ma liste" << endl;
    }

    if (!est_vide(&tableau))
    {
        cout << "Oups y a une anguille dans mon tableau" << endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        cout << "Oups y a une anguille dans ma liste" << endl;
    }

    if (est_vide(&tableau))
    {
        cout << "Oups y a une anguille dans mon tableau" << endl;
    }

    cout << "Elements initiaux:" << endl;
    affiche(&liste);
    affiche(&tableau);
 cout << endl;

    cout << "5e valeur de la liste " << recupere(&liste, 4) << endl;
    cout << "5e valeur du tableau " << recupere(&tableau, 4) << endl;

    cout << "21 se trouve dans la liste à " << cherche(&liste, 21) <<endl;
    cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    cout << "Elements après stockage de 7:" << endl;
    affiche(&liste);
    affiche(&tableau);
    cout << endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        cout << retire_file(&file) << endl;
        compteur--;
    }

    if (compteur == 0)
    {
        cout << "Ah y a un soucis là..." << endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        cout << retire_pile(&pile) << endl;
        compteur--;
    }

    if (compteur == 0)
    {
        cout << "Ah y a un soucis là..." << endl;
    }

    return 0;
}
