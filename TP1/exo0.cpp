#include <time.h>
#include <iostream>
using namespace std;

int sum(int n)
{
    if (n > 1)
    {
        return n + sum(n-1);
    }
    return n;
}

int main()
{

    std::cout<<sum(4);
    return 0;
}
